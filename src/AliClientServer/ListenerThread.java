package AliClientServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ListenerThread extends Thread {
    ServerFrame serverFrame;
    ServerSocket serverSocket;
    int port;
    Boolean exit;

    ListenerThread(ServerFrame serverFrame, int port) {
        this.serverFrame = serverFrame;
        this.port=port;
    }

    public void run() {
        try {
            serverSocket = new ServerSocket(port);
            while(isInterrupted()||serverSocket.isClosed()==false) {
                Socket socket1=serverSocket.accept();
                System.out.println("Player1 joined the game...");
                Socket socket2=serverSocket.accept();
                System.out.println("Player2 joined the game...");
                GameHandler handler=new GameHandler(socket1,socket2);
                handler.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
