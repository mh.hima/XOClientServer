package AliClientServer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class ClientHandler {
    Scanner scanner;
    PrintStream printer;
    Socket clientSocket;
    int playerIndex;

    ClientHandler(Socket clientSocket,int playerIndex) {
        this.clientSocket=clientSocket;
        this.playerIndex=playerIndex;
        initialize();
    }

    void initialize() {
        try {
            printer=new PrintStream(new BufferedOutputStream(clientSocket.getOutputStream()));
            printer.println("Hi");
            printer.flush();
            scanner = new Scanner(new BufferedInputStream(clientSocket.getInputStream()));
            scanner.nextLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(String str) {
        printer.println(str);
        printer.flush();
    }

    void sendStatus(String board[][]) {
        send(""+playerIndex);
        for(int i=0;i<3;i++) {
            for (int j = 0; j < 3; j++) {
                send(board[i][j]);
            }
        }
    }

    public String[][] receive() {
        String result[][]=new String[3][3];
        for(int i=0;i<3;i++) {
            for (int j = 0; j < 3; j++) {
                result[i][j]=scanner.nextLine();
            }
        }
        return result;
    }
}
