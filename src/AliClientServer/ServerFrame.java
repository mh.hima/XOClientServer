package AliClientServer;

import javax.swing.*;
import java.awt.*;
import java.net.ServerSocket;

public class ServerFrame extends JFrame {

    ListenerThread listenerThread;
    int port=9099;

    public ServerFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(new Dimension(500, 500));
        setTitle("Game Server");
        initialize();
    }

    void initialize() {
        listenerThread = new ListenerThread(this, port);
        listenerThread.start();
    }
}
