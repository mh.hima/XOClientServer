package AliClientServer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class ClientThread extends Thread {
    String serverIP;
    int port;
    Socket socket;
    ClientFrame frame;
    Scanner scanner;
    PrintStream printer;

    ClientThread(String serverIP,int port, ClientFrame frame) {
        this.serverIP=serverIP;
        this.frame=frame;
        this.port=port;
    }

    public void run() {
        try {
            frame.setEnabled(false);
            socket = new Socket(serverIP,port);

            printer=new PrintStream(new BufferedOutputStream(socket.getOutputStream()));
            printer.println("Hi");
            printer.flush();
            scanner = new Scanner(new BufferedInputStream(socket.getInputStream()));
            scanner.nextLine();

            while(isInterrupted()==false&&socket.isConnected()) {
                String str=scanner.nextLine();
                if(str.equals("Turn")) {
                    frame.setPlayer(Integer.parseInt(scanner.nextLine()));
                    frame.setEnabled(true);
                    frame.load(scanner);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void sendStatus() {
        frame.save(printer);
    }
}
