package AliClientServer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.Scanner;

public class ClientFrame extends JFrame {
    JButton board[][]=new JButton[3][3];
    String playerCharacter;
    JPanel mainPanel;
    String serverIP="127.0.0.1";
    int port=9099;
    ClientThread clientThread;

    public ClientFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(new Dimension(500,500));
        setTitle("Game Client");
        initialize();
    }

    void setPlayer(int player) {
        playerCharacter = (player==0)?"X":"O";
    }

    void initialize() {
        Container contentPain=getContentPane();
        mainPanel=new JPanel(new GridLayout(3,3));
        contentPain.add(mainPanel);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                final int r=i,c=j;
                board[i][j]=new JButton();
                board[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(board[r][c].getText().equals("")) {
                            board[r][c].setText(playerCharacter);
                            clientThread.sendStatus();
                            ClientFrame.this.setEnabled(false);
                        }
                    }
                });
                mainPanel.add(board[i][j]);
            }
        }

        clientThread=new ClientThread(serverIP,port,this);
        clientThread.start();
    }

    public void load(Scanner scanner) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j].setText(scanner.nextLine());
            }
        }
    }

    public void save(PrintStream printer) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                printer.println(board[i][j].getText());
                printer.flush();
            }
        }
    }
}
