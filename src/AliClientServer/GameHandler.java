package AliClientServer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class GameHandler extends Thread{
    Socket firstSocket, secondSocket;
    ClientHandler players[]=new ClientHandler[2];
    String board[][]=new String[3][3];

    public GameHandler(Socket firstSocket, Socket secondSocket) {
        this.firstSocket = firstSocket;
        this.secondSocket = secondSocket;
        players[0]=new ClientHandler(firstSocket,0);
        players[1]=new ClientHandler(secondSocket,1);
    }

    public void run() {
        int currentPlayer = 1;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = "";
            }
        }
        while (isInterrupted() == false && firstSocket.isConnected() && secondSocket.isConnected()) {
            players[currentPlayer].send("Turn");
            players[currentPlayer].sendStatus(board);
            System.out.println("Player"+(currentPlayer+1)+"'s Turn");
            String str[][] = players[currentPlayer].receive();
            if(apply(str)) {
                System.out.println("Valid Move");
                board=str;
                currentPlayer=1-currentPlayer;
            }
            else {
                System.out.println("Invalid Move");
            }
        }
    }

    boolean apply(String str[][]) {
        return true;
    }
}
