package AliClientServer;

import javax.swing.*;
import java.awt.*;

public class MainServer {
    public static void main(String[] args) {
        ServerFrame frame=new ServerFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(500,500));
        frame.setVisible(true);
    }
}
